#include "constants.h"
#include "utility.h"
#include "Timer.h"

#include <chrono>
#include <iostream>
#include <iomanip>
#include <thread>

//------------------------------ stringToWcs() ---------------------------------
// Utility to convert std::string to wide character string
//
std::wstring stringToWcs( const std::string& val )
{
  std::unique_ptr<wchar_t> wcs(new wchar_t[val.length()+1]);
  std::mbstowcs(wcs.get(), val.c_str(), val.length()+1);
  return wcs.get();
}
// End of stringToWcs()


//------------------------------ wcsToString() ---------------------------------
// Utility to convert wide character string to std::string
//
std::string wcsToString( const ::AT_WC* wcs ) 
{
  std::wstring ws( wcs );
  return std::string( ws.begin(), ws.end() );
}
// End of wcsToString()

//------------------------ AllocateAndQueueBuffers() ---------------------------
//
int AllocateAndQueueBuffers( AT_H H, std::vector<image_t>& frameBuffers )
{
  AT_64 imageSizeBytes;

  RETURN_ON_ERROR(AT_GetInt(H, L"ImageSizeBytes", &imageSizeBytes));

  for(int i=0; i<BUFFERCOUNT; i++)
  {
    image_t image(new AT_U8[imageSizeBytes]);
    int err = AT_QueueBuffer(H, image.get(), static_cast<int>(imageSizeBytes));
    if( err != AT_SUCCESS) {
      std::cout << "AT_QueueBuffer return code" << err << std::endl;
    }

    frameBuffers.push_back(move(image));
  }
}
// End of AllocateAndQueueBuffers()

int DoGenericConfiguration(AT_H H,
                           const std::string& gainMode, 
                           const std::string& shutterMode)
{
  return AT_SUCCESS;
}

//-------------------------------- Aoi2Roi() -----------------------------------
// Converts Andor AOI(top,left,width,height) to 
//            CSS ROI(originX,originY,sizeX,sizeY).
ROI Aoi2Roi(AOI& aoi, int sensorHeight)
{
  int originX( aoi.left - 1 );
  int originY( sensorHeight - aoi.height - aoi.top + 1 );
  int sizeX( aoi.width );
  int sizeY( aoi.height );

  return ROI(originX, originY, sizeX, sizeY, aoi.desc);
  
}
// End of Aoi2Roi()


//-------------------------------- Roi2Aoi() -----------------------------------
// Converts CSS ROI to Andor AOI(top,left,width,height) to 
//            CSS ROI(originX,originY,sizeX,sizeY).
AOI Roi2Aoi(ROI& roi, int sensorHeight)
{
  int left( roi.originX + 1 );
  int top( sensorHeight - (roi.originY + roi.sizeY) + 1 );
  int width( roi.sizeX );
  int height( roi.sizeY );

  return AOI(left, top, width, height, roi.desc);
  
}
// End of Roi2Aoi()


//---------------------------- IdentifyCamera() --------------------------------
//
int IdentifyCamera(AT_H H, int& cameraId)
{
  std::string cameraName;
  RETURN_ON_ERROR( GetString(H, L"CameraName", cameraName) );
  if( cameraName.find("Balor") != std::string::npos )
  {
    cameraId = BALOR;
    RETURN_ON_ERROR( PrintStringFeature(H, L"CameraInformation") );
  }
  else if( cameraName.find("Zyla") != std::string::npos )
  {
    cameraId = ZYLA;
    RETURN_ON_ERROR( PrintStringFeature(H, L"CameraModel") );
    RETURN_ON_ERROR( PrintStringFeature(H, L"CameraName") );
    RETURN_ON_ERROR( PrintStringFeature(H, L"SerialNumber") );
    RETURN_ON_ERROR( PrintStringFeature(H, L"FirmwareVersion") );
    RETURN_ON_ERROR( PrintIntFeature(H, L"SensorHeight") );
    RETURN_ON_ERROR( PrintIntFeature(H, L"SensorWidth") );
  }
  else
  {
    std::cout << "DON'T KNOW HOW TO HANDLE CAMERA NAME \"" 
              << cameraName << "\"!" << std::endl;
    return 1;
  }
 
 return AT_SUCCESS;
}
// End of IdentifyCamera()


//---------------------------- OpenAndIdentify() -------------------------------
//
int OpenAndIdentify( CamInfo& info )
{
  int err;

  RETURN_ON_ERROR(AT_InitialiseLibrary());

  AT_64 devcount;
  WARN_ON_ERROR(AT_GetInt(AT_HANDLE_SYSTEM, L"DeviceCount", &devcount));
  std::cout << "Found " << devcount << " devices" << std::endl;

  AT_H H;
  err = AT_Open(0, &H);

  if( AT_SUCCESS == err )
    std::cout << "Successfully Opened Camera"<< std::endl;
  else
  {
    std::cout << "AT_Open return code: =" << err << std::endl;
    WARN_ON_ERROR(AT_FinaliseLibrary());
    return err;
  }

  int id;
  RETURN_ON_ERROR( IdentifyCamera(H, id) );

  AT_64 w,h;
  RETURN_ON_ERROR( AT_GetInt(H, L"SensorWidth", &w) );
  RETURN_ON_ERROR( AT_GetInt(H, L"SensorHeight", &h) );

  info = CamInfo( H, id, w, h );

  return AT_SUCCESS;
}
// End of OpenAndIdentify()


//------------------------ GetLongExposureTranstion() --------------------------
//
int GetLongExposureTransition(
  const CamInfo& camInfo, 
  const bool debugEnabled,
  const AOI& aoi,  
  double& let,
  int& rowsRead )
{
  std::vector<AOI> AOIs;
  AOIs.emplace_back( aoi );
  return GetLongExposureTransition(camInfo, debugEnabled, AOIs, let, rowsRead);
}


//------------------------ GetLongExposureTranstion() --------------------------
//
int GetLongExposureTransition(
  const CamInfo& camInfo,
  const bool debugEnabled,
  const std::vector<AOI>& AOIs,  
  double& let,
  int& rowsRead)
{
  AT_H H(camInfo.getHandle());
  if( debugEnabled )
    std::cout << "GetLongExposureTransition: " << std::endl;
  if( camInfo.getCameraId() == BALOR )
  {
    double tmp;
    RETURN_ON_ERROR( AT_GetFloat(H, L"LongExposureTransition", &tmp) );
    let = tmp;
  }
  else 
  {
    double rowReadTime( 2624.0 * (1.0/284000000.0 ) );
               
    AT_BOOL fastAOIFrameRateEnable;
    RETURN_ON_ERROR( AT_GetBool(H, L"FastAOIFrameRateEnable", &fastAOIFrameRateEnable) );

    int half_1(0);
    int half_2(0);
    int halfHeight(camInfo.getSensorHeight()/2);

    for( AOI aoi : AOIs )
    {
      ROI roi( Aoi2Roi(aoi, camInfo.getSensorHeight()) );
      if( debugEnabled )
        std::cout << "  AOI: " << aoi << " == ROI: " << roi << std::endl;

      if( roi.originY < halfHeight )
      {
        if( (roi.originY + roi.sizeY) <= halfHeight )
        {
          half_1 += roi.sizeY;
        }
        else
        {
          int rowsInFirstHalf = halfHeight - roi.originY;
          half_1 += rowsInFirstHalf;
          half_2 += roi.sizeY - rowsInFirstHalf;
        }
      } 
      else
      {
        half_2 += roi.sizeY;
      }
    }
    rowsRead = (half_1 >= half_2) ? half_1 : half_2; 

    // For CSS purposes a single AOI/ROI will always use the 
    // AOI features of the camera.
    if( AOIs.size() == 1 )
    {
      // For the Zyla, when the number of rows read are < 1/2 SensorHeight
      // then the camera always reads an even number of rows.
      if( rowsRead%2 != 0 ) rowsRead++;
      if( !fastAOIFrameRateEnable ) rowsRead += 2;
    }
    else
    {
      if( rowsRead%2 != 0 ) rowsRead++;
    }

    if( debugEnabled )
    {
      std::cout << "  Rows in half_1=" << half_1
                << ", Rows in half_2=" << half_2
                << ", rowsRead=" << rowsRead << std::endl;
    }

    let = (rowsRead * rowReadTime);// + (4.0 * rowReadTime); // + (AOIs.size() * 2.0 * rowReadTime);

  }

  if( debugEnabled)
    std::cout << "  LongExposureTransition: " << std::fixed << std::setprecision(6) 
              << let << "s" << std::endl;

  return AT_SUCCESS;
}
// End of GetLongExposureTransition

//----------------------------- parseMetadata() --------------------------------
//
Metadata parseMetadata(unsigned char *image_data, int image_size, bool verbose)
{

  unsigned char* metaData = image_data + image_size;

  // Read 1st Metadata blob from end of buffer using MetadataHeader union
  MetadataHeader mdHeader;
  Metadata metadata;

  do
  {
    // Get CID/Length field of metadata blob
    mdHeader.word = *(reinterpret_cast<std::uint64_t*>(metaData - (LENGTH_FIELD_SIZE + CID_FIELD_SIZE)));
    if( verbose )
      std::wcout << L"Metadata: CID=" << mdHeader.field.cid
                 << L" Length= " << mdHeader.field.length << std::endl;
 
    // Point to beginning of data for this metadata blob
    metaData = metaData - (mdHeader.field.length + LENGTH_FIELD_SIZE);

    switch( mdHeader.field.cid ) {
      case CID_FRAMEINFO:
        metadata.frameInfo.word = *(reinterpret_cast<std::uint64_t*>(metaData));
        if( verbose )
        {
          std::wcout << L"CID " << mdHeader.field.cid << L"=FrameInfo" << std::endl;
          std::wcout << L"  frameInfo.stride: " << metadata.frameInfo.field.stride << std::endl;
          std::wcout << L"  frameInfo.pixelEncoding: " << metadata.frameInfo.field.pixelEncoding << std::endl;
          std::wcout << L"  frameInfo.aoiWidth: " << metadata.frameInfo.field.aoiWidth << std::endl;
          std::wcout << L"  frameInfo.aoiHeight: " << metadata.frameInfo.field.aoiHeight << std::endl;
        }
        break;

      case CID_TIMESTAMP:
        metadata.timestamp = *(reinterpret_cast<std::uint64_t*>(metaData));
        if( verbose )
        {
          std::wcout << L"CID " << mdHeader.field.cid << L"=Timestamp" << std::endl;
          std::wcout << L"  timestamp: " << metadata.timestamp << std::endl;
        }
        break;

      case CID_FRAME:
        metadata.data.length = mdHeader.field.length - CID_FIELD_SIZE;
        if( verbose )
        {
          std::wcout << L"CID " << mdHeader.field.cid << L"=FrameData" << std::endl;
          std::wcout << L"  length=" << metadata.data.length << std::endl;
          std::wcout << L"  image_data=" << static_cast<void*>(image_data)
                     << " (original buffer pointer)" << std::endl;
          std::wcout << L"  metaData  =" << static_cast<void*>(metaData)
                     <<  " (ptr to beginning of CID 0 frame data)" << std::endl;
          if( image_data != metaData )
            std::wcout << L"ERROR - Mismatch between expected image data pointer"
                       << L"(image-data) and pointer derived from meta-data!";
        }
        break;

      default:
        if( verbose )
          std::wcout << L"Don't know how to handle CID=" << mdHeader.field.cid << std::endl;
    }
  } while( mdHeader.field.cid != 0 );

  return metadata;
}
// End of parseMetadata()


//------------------------- SetBasicConfiguration() ----------------------------
//
int SetBasicConfiguration(const CamInfo& camInfo)
{
  AT_H H( camInfo.getHandle() );

  std::cout << "-------- Setting Basic Configuration --------" 
            << std::endl;

  if( camInfo.getCameraId() == ZYLA )
  {
    RETURN_ON_ERROR( PrintAndSetBoolFeature(H, L"Overlap", AT_FALSE) );
  }
  RETURN_ON_ERROR( AT_SetBool(H, L"DirectQueueing", AT_TRUE) );
  RETURN_ON_ERROR( PrintBoolFeature(H, L"DirectQueueing") );
  std::cout << "Number of Buffers: " << BUFFERCOUNT << std::endl;
  
  WaitForSensorInitialisation(H);

  RETURN_ON_ERROR( PrintEnumFeature(H, L"PixelReadoutRate") );
  RETURN_ON_ERROR( PrintFloatFeature(H, L"RowReadTime", 6) );
  RETURN_ON_ERROR( PrintFloatFeatureMinMax(H, L"ExposureTime", 6) );
  RETURN_ON_ERROR( PrintFloatFeatureMinMax(H, L"FrameRate", 17) );

  AT_BOOL isImplemented;
  RETURN_ON_ERROR( AT_IsImplemented(H,L"FastAOIFrameRateEnable",&isImplemented) );
  if( AT_TRUE == isImplemented )
    RETURN_ON_ERROR( PrintBoolFeature(H, L"FastAOIFrameRateEnable") );

  RETURN_ON_ERROR( PrintBoolFeature(H,L"FullAOIControl") );
  RETURN_ON_ERROR( PrintEnumFeature(H,L"AOIBinning") );
  RETURN_ON_ERROR( PrintIntFeature(H,L"AOIHBin") );
  RETURN_ON_ERROR( PrintIntFeature(H,L"AOIVBin") );
  RETURN_ON_ERROR( PrintIntFeatureMinMax(H,L"AOILeft") );
  RETURN_ON_ERROR( PrintIntFeatureMinMax(H,L"AOITop") );
  RETURN_ON_ERROR( PrintIntFeatureMinMax(H,L"AOIWidth") );
  RETURN_ON_ERROR( PrintIntFeatureMinMax(H,L"AOIHeight") );
  RETURN_ON_ERROR( PrintIntFeatureMinMax(H,L"MultitrackStart") );
  RETURN_ON_ERROR( PrintIntFeatureMinMax(H,L"MultitrackEnd") );

  RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"ElectronicShutteringMode", DEFAULT_SHUTTERMODE) );
  RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"TriggerMode", DEFAULT_TRIGGERMODE) );
  RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"CycleMode", DEFAULT_CYCLEMODE) );
  RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"PixelEncoding", DEFAULT_PIXELENCODING) );
  RETURN_ON_ERROR( PrintAndSetIntFeature(H,L"FrameCount", DEFAULT_FRAMESTOACQUIRE) );
  RETURN_ON_ERROR( PrintAndSetFloatFeature(H,L"ExposureTime", DEFAULT_EXPOSURETIME) );

//  RETURN_ON_ERROR( PrintAndSetFloatFeature(H, L"ExposureTime", DEFAULT_EXPOSURETIME) );
  RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"AOILayout", DEFAULT_AOILAYOUT ) );

  std::cout << "-------- Setting Basic Configuration Complete! --------" 
            << std::endl << std::endl;

  return AT_SUCCESS;
}
// End of SetBasicConfiguration()


//--------------------------- PrintMono16Frame() -------------------------------
//
void PrintMono16Frame( int frameNumber, unsigned char* frameP, 
                       int imageSizeBytes, int aoistride, 
                       int pixelCount, int rowCount )
{
  std::cout << "  Frame " << frameNumber 
            << ", ImageSize " << imageSizeBytes << std::endl;

  for(int row=0; row<rowCount; row++) {
    std::cout << "    Row " << std::dec 
              << std::setfill(' ') << std::setw(4) << (row+1) << ": " << std::hex;
    unsigned short *pixels = reinterpret_cast<unsigned short *>(frameP+ aoistride*row);
    for (int i = 0; i < pixelCount; i++) {
      std::cout << "0x" << std::setfill('0') << std::setw(4) << *pixels++;
      if (i < pixelCount-1) {
        std::cout << ", ";
      }
    }
    std::cout << std::endl;
  }

  std::cout << "    Last Row End Pixels: ";

  unsigned short *pixels = reinterpret_cast<unsigned short*>(frameP+imageSizeBytes-pixelCount*2);
  for (int i=0; i<pixelCount; i++) {
    std::cout << "0x" << std::setfill('0') << std::setw(4) << *pixels++;
    if (i<pixelCount-1) {
      std::cout << ", ";
    }
  }

  std::cout << std::endl << std::dec;
}
// End of PrintMono16Frame()




int PrintBoolFeature(AT_H H, const std::wstring& feature) {
  std::wostringstream wMsgStr;
  AT_BOOL value;

  RETURN_ON_ERROR(AT_GetBool(H, feature.c_str(), &value));

  wMsgStr << feature << L": " << value << std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return AT_SUCCESS;
}

int PrintEnumFeature(AT_H H, const AT_WC* feature) {
  int enumIndex;
  std::wostringstream wMsgStr;

  RETURN_ON_ERROR(AT_GetEnumIndex(H, feature, &enumIndex));

  AT_WC enumString[40];
  RETURN_ON_ERROR(AT_GetEnumStringByIndex(H, feature, enumIndex, enumString, 40));

  wMsgStr<<feature<<L": "<<enumString<<L"["<<enumIndex<<"]"<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return AT_SUCCESS;
}

int PrintEnumFeatureIndexInfo(AT_H H, const AT_WC* feature, int enumIndex) {
  std::wostringstream wMsgStr;
  AT_BOOL isAvailable(AT_FALSE);
  AT_BOOL isImplemented(AT_FALSE);
  AT_WC enumString[40];

  RETURN_ON_ERROR(AT_IsEnumIndexImplemented(H, feature, enumIndex, &isImplemented));
  if( isImplemented )
  {
    RETURN_ON_ERROR(AT_IsEnumIndexAvailable(H, feature, enumIndex, &isAvailable));
    RETURN_ON_ERROR(AT_GetEnumStringByIndex(H, feature, enumIndex, enumString, 40));
  }

  wMsgStr << feature 
          << L": index=" << enumIndex
          << L", isImplemented=" << isImplemented
          << L", isAvailable=" << isAvailable;
  if( isImplemented )
    wMsgStr << L", value=\"" << enumString << L"\"" << std::endl;
  else
    wMsgStr << L", NO VALUE" << std::endl;

  std::wcout << wMsgStr.str() << std::flush;
  return AT_SUCCESS;
}

int PrintFloatFeature(AT_H H, const AT_WC* feature, const int precision) {
  double value;
  std::ostringstream msgStr;

  RETURN_ON_ERROR(AT_GetFloat(H, feature, &value));

  msgStr << std::fixed << std::setprecision(precision) 
         << wcsToString(feature) << ": " << value << std::endl;
  std::cout << msgStr.str() << std::flush;

  return AT_SUCCESS;
}

int PrintFloatFeatureMinMax(AT_H H, const AT_WC* feature, const int precision) {
  double min, max;
  std::ostringstream msgStr;

  RETURN_ON_ERROR(AT_GetFloatMin(H, feature, &min));
  RETURN_ON_ERROR(AT_GetFloatMax(H, feature, &max));

  msgStr << std::fixed << std::setprecision(precision)
         << wcsToString(feature) << ": Min="  << min <<", Max=" << max <<std::endl;
  std::cout << msgStr.str() << std::flush;

  return AT_SUCCESS;
}

int PrintIntFeature(AT_H H, const AT_WC* feature) {
  int length = 0;
  std::wostringstream wMsgStr;
  AT_64 value;

  RETURN_ON_ERROR(AT_GetInt(H, feature, &value));

  wMsgStr<<feature<<L": "<<value<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return AT_SUCCESS;
}

int PrintIntFeatureMinMax(AT_H H, const AT_WC* feature) {
  std::wostringstream wMsgStr;
  AT_64 min, max;

  RETURN_ON_ERROR(AT_GetIntMin(H, feature, &min));
  RETURN_ON_ERROR(AT_GetIntMax(H, feature, &max));

  wMsgStr<<feature<<L": Min="<<min<<", Max=" <<max<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return AT_SUCCESS;
}

int GetString(AT_H H, const AT_WC* feature, std::string& value)
{
  std::wostringstream wMsgStr;
  int length = 0;

  RETURN_ON_ERROR(AT_GetStringMaxLength(H, feature, &length));
  std::vector<AT_WC> stringValue(static_cast<size_t>(length));
  RETURN_ON_ERROR(AT_GetString(H, feature, stringValue.data(), static_cast<int>(stringValue.size())));
  value = wcsToString(&stringValue[0]);
  return AT_SUCCESS;
}

int PrintStringFeature(AT_H H, const AT_WC* feature) {
  std::wostringstream wMsgStr;
  int length = 0;

  RETURN_ON_ERROR(AT_GetStringMaxLength(H, feature, &length));

  std::vector<AT_WC> stringValue(static_cast<size_t>(length));
  RETURN_ON_ERROR(AT_GetString(H, feature, stringValue.data(), static_cast<int>(stringValue.size())));
  wMsgStr<<feature<<L": "<<&stringValue[0]<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return AT_SUCCESS;
}

int PrintAndSetBoolFeature(AT_H H, const AT_WC* feature, AT_BOOL value)
{
  std::wostringstream wMsgStr;

  wMsgStr<<L"Setting "<<feature<<L" to "<<value<<" ... ";
  int err = AT_SetBool(H,feature,value);
  if(AT_SUCCESS == err) {
    wMsgStr<<L"OK";
  }
  else {
    wMsgStr<<L"Error - "<<err;
  }
  wMsgStr<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return err;
}

int PrintAndSetEnumFeature(AT_H H, const AT_WC* feature, const AT_WC* value)
{
  std::wostringstream wMsgStr;

  wMsgStr<<L"Setting "<<feature<<L" to "<<value<<" ... ";
  int err = AT_SetEnumString(H,feature,value);
  if(AT_SUCCESS == err) {
    wMsgStr<<L"OK";
  }
  else {
    wMsgStr<<L"Error - "<<err;
  }
  wMsgStr<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return err;
}

int PrintAndSetFloatFeature(AT_H H, const AT_WC* feature, double value)
{
  std::wostringstream wMsgStr;

  wMsgStr<<L"Setting "<<feature<<L" to "<<value<<" ... ";
  int err = AT_SetFloat(H,feature,value);
  if(AT_SUCCESS == err) {
    wMsgStr<<L"OK";
  }
  else {
    wMsgStr<<L"Error - "<<err;
  }
  wMsgStr<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return err;
}

int PrintAndSetIntFeature(AT_H H, const AT_WC* feature, AT_64 value)
{
  PrintAndSetIntFeature(H, feature, value, false);
}

int PrintAndSetIntFeature(AT_H H, const AT_WC* feature, AT_64 value, const bool inHex)
{
  std::wostringstream wMsgStr;

  wMsgStr << L"Setting " << feature << L" to ";
  if( inHex ) 
    wMsgStr << L"0x" << std::hex;
  else
    wMsgStr << std::dec;

  wMsgStr << value << " ... ";

  int err = AT_SetInt(H,feature,value);
  if(AT_SUCCESS == err) {
    wMsgStr<<L"OK";
  }
  else {
    wMsgStr<<L"Error - "<<err;
  }
  wMsgStr<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return err;
}

int PrintIsImplemented(AT_H H, const AT_WC* feature) {
  std::wostringstream wMsgStr;
  AT_BOOL value;

  RETURN_ON_ERROR(AT_IsImplemented(H, feature, &value));

  wMsgStr<<feature<<L" isImplemented: "<<value<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return AT_SUCCESS;
}

int PrintIsReadable(AT_H H, const std::wstring& feature) {
  std::wostringstream wMsgStr;
  AT_BOOL value;

  RETURN_ON_ERROR(AT_IsReadable(H, feature.c_str(), &value));

  bool val = (value == AT_TRUE) ? true : false;
  wMsgStr<<feature<<L" isReadable: "<<std::boolalpha<<value<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return AT_SUCCESS;
}

int PrintIsReadOnly(AT_H H, const std::wstring& feature) {
  std::wostringstream wMsgStr;
  AT_BOOL value;

  RETURN_ON_ERROR(AT_IsReadOnly(H, feature.c_str(), &value));

  wMsgStr<<feature<<L" isReadOnly: "<<value<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return AT_SUCCESS;
}

int PrintIsWritable(AT_H H, const std::wstring& feature) {
  std::wostringstream wMsgStr;
  AT_BOOL value;

  RETURN_ON_ERROR(AT_IsWritable(H, feature.c_str(), &value));

  wMsgStr<<feature<<L" isWritable: "<<value<<std::endl;
  std::wcout << wMsgStr.str() << std::flush;
  return AT_SUCCESS;
}

// --------------------- WaitForSensorInitialisation() -------------------------
//
int WaitForSensorInitialisation(AT_H H) {
  int status(AT_SUCCESS);
  AT_BOOL sensorInitialised(AT_FALSE);
  AT_BOOL sensorInitialisedImplemented(AT_FALSE);

  AT_IsImplemented(H, L"SensorInitialised", &sensorInitialisedImplemented);
  if (sensorInitialisedImplemented == AT_TRUE) {
  {
    std::wcout << L"Waiting for SensorInitialised.";
    std::wcout.flush();
  }

    Timer t(10000);
    t.start();
    double elapsedDotsTime(0.5);
    do {
      if( t.get_elapsed() >= elapsedDotsTime )
      {
        std::wcout << L".";
        std::wcout.flush();
        elapsedDotsTime += 0.5;
      }

      RETURN_ON_ERROR( status = AT_GetBool(H, L"SensorInitialised", &sensorInitialised) );

    } while( sensorInitialised == AT_FALSE && !t.didTimeout() );

    if( t.didTimeout() )
      status = AT_ERR_TIMEDOUT;
    else if( status == AT_SUCCESS )
    {
      t.stop();
      std::wcout << L"OK, completed in " << std::fixed << t.get_dt() << L"sec" << std::endl;
    }
  }

  return status;
}
// End of WaitForSensorInitialisation()


// ----------------------- WaitForCorrectionApplying() --------------------------
//
int WaitForCorrectionApplying(AT_H H, int timeout_ms)
{
  int status(AT_SUCCESS);
  double elapsedDotsTime(0.5); 
  AT_BOOL correction_applying(AT_TRUE);
  Timer t(timeout_ms);

  std::wcout << L"Waiting for CorrectionApplying.";
  std::wcout.flush();

  t.start();
  do {
    if( t.get_elapsed() >= elapsedDotsTime )
    {
      std::wcout << L".";
      std::wcout.flush();
      elapsedDotsTime += 0.5;
    }

    if( elapsedDotsTime > 0.5 )
      RETURN_ON_ERROR(AT_GetBool(H, L"CorrectionApplying", &correction_applying));

  } while( correction_applying == AT_TRUE && !t.didTimeout() );

  if( t.didTimeout() )
  {
    std::wcout << L"FAILURE: Timeout after " << timeout_ms << L"ms, ";
    PrintBoolFeature(H, L"CorrectionApplying");
    status = AT_ERR_TIMEDOUT;
  }
  else
  {
    t.stop();
    std::wcout << L"OK, completed in " << t.get_dt() << L"sec." << std::endl;
  }
  return status;
}
// End of Wait for CorrectionApplying()
