#ifndef ANDOR_UTILITY_H
#define ANDOR_UTILITY_H

#include "atcore.h"
#include "constants.h"

#include <iomanip>
#include <memory>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

typedef std::unique_ptr<AT_U8[]> image_t;
class AOI;
class ROI;

class AOI {
  public:
    int left;
    int top;
    int width;
    int height;
    std::string desc;
    AOI() : left(1), top(1), width(1), height(1), desc("UNDEF") {}
    AOI(int l, int t, int w, int h, std::string d) : 
      left(l), top(t), width(w), height(h), desc(d) {}

    AOI& operator=(const AOI& rhs)
    {
      if( this == &rhs )            // check for assignment to self
        return *this;
      left   = rhs.left;
      top    = rhs.top;
      width  = rhs.width;
      height = rhs.height;
      desc   = rhs.desc;
      return *this;
    }

    std::string toSimpleString()
    {
      std::ostringstream msgStr;
      msgStr << std::right << std::setw(4) << left 
             << ", " << std::right << std::setw(4) << top
             << ", " << std::right << std::setw(4) << width
             << ", " << std::right << std::setw(4) << height;
      return msgStr.str();
    }

    friend std::ostream& operator<<(std::ostream& os, const AOI& rhs)
    {
      os << rhs.desc << ": " 
         << "Left=" << std::right << std::setw(4) << rhs.left 
         << ", Top=" << std::right << std::setw(4) << rhs.top
         << ", Width=" << std::right << std::setw(4) << rhs.width
         << ", Height=" << std::right << std::setw(4) << rhs.height;
      return os;
    }
};

class ROI {
  public:
    int originX;
    int originY;
    int sizeX;
    int sizeY;
    std::string desc;
    ROI() : originX(0), originY(0), sizeX(1), sizeY(1), desc("UNDEF") {}
    ROI(int oX, int oY, int sX, int sY, std::string d) : 
      originX(oX), originY(oY), sizeX(sX), sizeY(sY), desc(d) {}

    ROI& operator=(const ROI& rhs)
    {
      if( this == &rhs )            // check for assignment to self
        return *this;
      originX = rhs.originX;
      originY = rhs.originY;
      sizeX   = rhs.sizeX;
      sizeY   = rhs.sizeY;
      desc    = rhs.desc;
      return *this;
    }

    std::string toSimpleString()
    {
      std::ostringstream msgStr;
      msgStr << std::right << std::setw(4) << originX 
             << ", " << std::right << std::setw(4) << originY
             << ", " << std::right << std::setw(4) << sizeX
             << ", " << std::right << std::setw(4) << sizeY;
      return msgStr.str();
    }

    friend std::ostream& operator<<(std::ostream& os, const ROI& rhs)
    {
      os << rhs.desc << ": " 
         << "OriginX=" << std::right << std::setw(4) << rhs.originX
         << ", OriginY=" << std::right << std::setw(4) << rhs.originY
         << ", SizeX=" << std::right << std::setw(4) << rhs.sizeX
         << ", SizeY=" << std::right << std::setw(4) << rhs.sizeY;
      return os;
    }
};


class Bin {
  public:
    int x;
    int y;
    Bin(int x, int y) : 
      x(x), y(y) {}

    Bin& operator=(const Bin& rhs)
    {
      if( this == &rhs )            // check for assignment to self
        return *this;
      x = rhs.x;
      y = rhs.y;
      return *this;
    }

    std::string toSimpleString()
    {
      std::ostringstream msgStr;
      msgStr << std::right << std::setw(4) << x 
             << ", " << std::right << std::setw(4) << y;
      return msgStr.str();
    }

    friend std::ostream& operator<<(std::ostream& os, const Bin& rhs)
    {
      os << "binX=" << std::right << std::setw(4) << rhs.x 
         << ", binY=" << std::right << std::setw(4) << rhs.y;
      return os;
    }
};

class CamInfo 
{
  AT_H H;                                  // Device handle
  int cameraId;                            // DKIST camera ID
  int sensorWidth;                         // As reported by camera
  int sensorHeight;                        // As reported by camera
  std::vector<std::string> gainModes;      // Supported/used by DKIST
  std::vector<std::string> shutterModes;   // Supported/used by DKIST
  AOI fullFrameAOI;
  ROI fullFrameROI;

  public: 
    CamInfo() : cameraId(0), H(0), sensorWidth(0), sensorHeight(0) { }
    CamInfo( AT_H handle, int id, int w, int h ) : 
      H(handle), 
      cameraId(id),
      sensorWidth(w),
      sensorHeight(h),
      gainModes( (id==BALOR) ? balorGainModes : zylaGainModes ),
      shutterModes( (id==BALOR) ? balorShutterModes : zylaShutterModes ) 
    { 
      fullFrameAOI = AOI(1,1,w,h,"AOI"); 
      fullFrameROI = ROI(0,0,w,h,"ROI");
    }

    int getCameraId() const { return cameraId; }
    AT_H getHandle() const { return H; }
    std::vector<std::string> getGainModes() const { return gainModes; }
    std::vector<std::string> getShutterModes() const { return shutterModes; }
    int getSensorWidth() const { return sensorWidth; }
    int getSensorHeight() const { return sensorHeight; }
    AOI getFullFrameAOI() const { return fullFrameAOI; }
    ROI getFullFrameROI() const { return fullFrameROI; }
};

typedef struct ReadWriteReadOnly {
  std::wstring name;
  AT_BOOL readable;
  AT_BOOL writable;
  AT_BOOL readonly;

  ReadWriteReadOnly(const AT_WC *n, AT_BOOL r, AT_BOOL w, AT_BOOL ro) {
    name = std::wstring(n);
    readable = r;
    writable = w;
    readonly = ro;
  }
} ReadWriteReadOnly;

typedef union {
  std::uint64_t word;
  struct {
    std::uint32_t cid;
    std::uint32_t length;
  } field;
} MetadataHeader;

typedef union {
  std::uint64_t word;
  struct {
    std::uint16_t stride;
    std::uint8_t  pixelEncoding;
    std::uint8_t  unused;
    std::uint16_t aoiWidth;
    std::uint16_t aoiHeight;
  } field;
} MetadataFrameInfo;

typedef struct {
  std::uint64_t length;
} MetadataData;

typedef struct {
  MetadataData data;
  MetadataFrameInfo frameInfo;
  std::uint64_t timestamp;
} Metadata;


//uncomment the line below to print the SDK calls and results from each call
//#define PRINTSDKCALLS

#define CAT(x, y)  x##y
#define WIDE(x)    CAT(L,x)

#ifdef PRINTSDKCALLS
#define RETURN_ON_ERROR(command) { int result = command; std::wcout<<__LINE__<<L"\t: "<<WIDE(#command)<<L" = "<<result <<std::endl; if(result!=AT_SUCCESS) return result; }
#define WARN_ON_ERROR(command)   { int result = command; std::wcout<<__LINE__<<L"\t: "<<WIDE(#command)<<L" = "<<result<<std::endl;}
#else
#define RETURN_ON_ERROR(command) { int result = command; if(result!=AT_SUCCESS) { std::wcout<<__LINE__<<L"\t: "<<WIDE(#command)<<L" returned error code "<<result<<std::endl; return result;} }
#define WARN_ON_ERROR(command)   { int result = command; if(result!=AT_SUCCESS) { std::wcout<<__LINE__<<L"\t: "<<WIDE(#command)<<L" returned error code "<<result<<std::endl;} }
#endif

std::wstring stringToWcs( const std::string& val );
std::string wcsToString( const ::AT_WC* wcs );

Metadata parseMetadata(unsigned char *image_data, int image_size, bool verbose);

int AllocateAndQueueBuffers(AT_H H, std::vector<image_t>& frameBuffers);
ROI Aoi2Roi(AOI& aoi);
AOI Roi2Aoi(ROI& roi);
int DoGenericConfiguration(AT_H H, const std::string& gainMode, 
                                   const std::string& shutterMode);

int IdentifyCamera(AT_H, int& cameraId);
int OpenAndIdentify( CamInfo& info );

int GetLongExposureTransition(const CamInfo& camInfo,
                              const bool debugEnabled,
                              const AOI& aoi, 
                              double& let,
                              int& rowsRead);
int GetLongExposureTransition(const CamInfo& camInfo, 
                              const bool debugEnabled,
                              const std::vector<AOI>& aois, 
                              double& let,
                              int& rowsRead);
int SetBasicConfiguration(const CamInfo& camInfo);

int GetString(AT_H H, const AT_WC* feature, std::string& value);

void PrintMono16Frame(int frameNumber, unsigned char* frameP, 
                      int imageSizeBytes, int aoistride, 
                      int pixelCount, int rowCount);

int PrintBoolFeature(AT_H H, const std::wstring& feature);
int PrintEnumFeature(AT_H H, const AT_WC* feature);
int PrintEnumFeatureIndexInfo(AT_H H, const AT_WC* feature, int enumIndex);
int PrintFloatFeature(AT_H H, const AT_WC* feature, const int precision=6);
int PrintFloatFeatureMinMax(AT_H H, const AT_WC* feature, const int precision=6);
int PrintIntFeature(AT_H H, const AT_WC* feature);
int PrintIntFeatureMinMax(AT_H H, const AT_WC* feature);
int PrintStringFeature(AT_H H, const AT_WC* feature);
int PrintAndSetBoolFeature(AT_H H, const AT_WC* feature, AT_BOOL value);
int PrintAndSetEnumFeature(AT_H H, const AT_WC* feature, const AT_WC* value);
int PrintAndSetFloatFeature(AT_H H, const AT_WC* feature, double value);
int PrintAndSetIntFeature(AT_H H, const AT_WC* feature, AT_64 value);
int PrintAndSetIntFeature(AT_H H, const AT_WC* feature, AT_64 value, const bool inHex);
int PrintIsReadable(AT_H H, const std::wstring& feature);
int PrintIsImplemented(AT_H H, const std::wstring& feature);
int PrintIsReadOnly(AT_H H, const std::wstring& feature);
int PrintIsWritable(AT_H H, const std::wstring& feature);

int WaitForCorrectionApplying(AT_H H, int timeout_ms);
int WaitForSensorInitialisation(AT_H H);

#endif
